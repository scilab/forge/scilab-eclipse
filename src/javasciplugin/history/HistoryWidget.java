package javasciplugin.history;


import java.util.Enumeration;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.*;
import org.eclipse.ui.*;
import org.eclipse.ui.ide.IDE;

import javasciplugin.Activator;
import javasciplugin.JavaSciWrapper;
import javasciplugin.Observer;
import javasciplugin.WrapperReceiver;
import javasciplugin.console.SciConsole;
import javasciplugin.console.myEditorInput;

/**
 * This class defines a SWT Widget that can be loaded as a part of a eclipse plugin.
 * It defines layout and the events of the Scilab history. It is composed of a SWT List, a Text and a Button which are placed on a GridLayout.
 * **/
public class HistoryWidget extends Composite implements Observer, WrapperReceiver{
	
	private List list;
	private Text text;
	private Button button;	  

	/**
	 * This constructor initialises the SWT components and puts them on a GridLayout.
	 * Furthermore the default selection(double-click on a item of the list) listener of the List and the Selection(single click on the Button) listener of the Button are initialised.
	 * The listener on the List sends the selected item (string) of the list to the console where it is shown as a input Text.
	 * The listener on the Button searches the history for every item that contains the text specified in the Text component. 
	 * It selects ever item that contains the text and sets the focus to the first item that is found.
	 * Furthermore the javaSciWrapper is requested from the Activator.
	 * @param parent is used to register the widget in the eclipse engine.
	 * @param style is used to register the widget in the eclipse engine.
	 * **/
	HistoryWidget(Composite parent, int style) {
		super(parent, style);

		GridData data;
		     
		text = new Text(this, SWT.SINGLE | SWT.BORDER);
		data=new GridData(GridData.FILL_HORIZONTAL);
		text.setLayoutData(data);
		
		button = new Button(this, SWT.PUSH);
		button.setText("Search");
		data=new GridData(GridData.HORIZONTAL_ALIGN_END);
		data.widthHint=55;
		button.setLayoutData(data);
		
		list = new List(this, SWT.MULTI | SWT.V_SCROLL |   SWT.H_SCROLL | SWT.BORDER);
		data=new GridData(GridData.FILL_BOTH);
		data.horizontalSpan = 2;
		list.setLayoutData(data);
		Font myFont= new Font(this.getParent().getDisplay(), "Arial", 8, SWT.NORMAL);
		list.setFont(myFont);

		Activator.getDefault().getWrapper(this);
		
		list.addListener (SWT.DefaultSelection, new Listener () {
			public void handleEvent (Event e) {
				String selection = list.getSelection()[0];
				try {
					myEditorInput input=new myEditorInput();
					IEditorPart editorPart = IDE.openEditor(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(), input,"javasciplugin.console.SciConsole");
					((SciConsole)editorPart).execute(selection);
		 		} catch (PartInitException e1) {
		 			e1.printStackTrace();
		 		}
			}
		});
		 	
		button.addListener (SWT.Selection, new Listener () {
			public void handleEvent (Event e) {
				list.deselectAll();
				for(int i = 0; i < list.getItemCount();i++){
					for(int j = 0;j<list.getItem(i).length();j++){
						if(list.getItem(i).indexOf(text.getText(), j)>-1){
							list.select(i);
							list.showSelection();
						}			
					}
				}
			}
		});
	}

	/**
	 * This method is part of the Observer interface. This interface is implemented by every class that the JavaSciWrapper to notify command executions.
	 * In consequence of a command execution the history has changed and need to be updated.
	 * @param pWrapper is the javaSciWrapper that calls this method
	 * **/
	public void update(JavaSciWrapper pWrapper) {
		list.removeAll();
	 	Enumeration enumHistory = pWrapper.getHistory();
	 	while(enumHistory.hasMoreElements()){
		    list.add(((String)enumHistory.nextElement()).trim());
		}
	}
	
	/**
	 * This method is part of the WrapperReceiver interface. This interface is implemented by every class that need the JavaSciWrapper when the plugin is started.
	 * The plugin Activator uses this method when the JavaSciWrapper is loaded. This allows the plugin to load the visual independent from the data.
	 * This makes the plugin stable on startup.
	 * @param w is the javaSciWrapper that calls this method
	 * **/
	public void receiveWrapper(final JavaSciWrapper w) {
		Display.getDefault().asyncExec(new Runnable() {
			public void run() {
				HistoryWidget.this.initHistory(w);
			}	
		});
	}
	
	/**
	 * This method is called by the WrapperReceiver method. It inserts the history in the List, starts the console and adds this widget as an Observer to the JavaSciWrapper.
	 * Now on every command execution the history is updated.
	 * @param the javaSciWrapper that calls the WrapperReceiver method
	 * **/
	private void initHistory(JavaSciWrapper w) {
		list.removeAll();
	 	Enumeration enumHistory = w.getHistory();
	 	while(enumHistory.hasMoreElements()){
		    list.add(((String)enumHistory.nextElement()).trim());
		}

	 	w.addObserver(HistoryWidget.this);
	 	Activator.getDefault().startConsole();
	}
}
