package javasciplugin.history;


import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;

/**
 * This class defines the console editor of the Scilab plugin.
 * A view is a basic container for some visual content in eclipse. This view contains and manages an instance of the HistoryWidget class.
 * **/
public class SciHistory  extends ViewPart{

		public HistoryWidget view;
		
		/**
		 * This method is used by the plugin to register this view on startup. It initialises the HistoryWidget and the GridLayout.
		 * @param frame is the parent frame in which the widget is show.
		 * **/
		public void createPartControl(Composite frame) {
			view =new HistoryWidget(frame, SWT.NONE);
			GridData myGridData = new GridData(GridData.HORIZONTAL_ALIGN_FILL | GridData.GRAB_HORIZONTAL | GridData.VERTICAL_ALIGN_FILL | GridData.GRAB_VERTICAL);
			view.setLayoutData(myGridData);
			GridLayout myGridLayout=new GridLayout();
			myGridLayout.numColumns=2;
			view.setLayout(myGridLayout);
		}
		
		/**
		 * This method is executed when the SciHistory gets the focus.
		 * **/
		public void setFocus() {
			view.setFocus();
		}
		
		/**
		 * This method is executed when the plugin is closed.
		 * **/
		public void dispose() {
			view.dispose();
			super.dispose();
		}
}
