package javasciplugin;

/**
 * This Interface is used by the ConsoleWidget, the HistoryWidget and the WorkspaceWidget on startup to get the JavaSciWrapper.
 * **/
public interface WrapperReceiver {
	/**
	 * This method is implemented by  the ConsoleWidget, the HistoryWidget and the WorkspaceWidget to receive the JavaSciWrapper during the startup of the plugin.
	 * **/
	void receiveWrapper(JavaSciWrapper w);
}