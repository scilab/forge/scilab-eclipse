package javasciplugin;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Vector;
import java.util.concurrent.Semaphore;

import javasci.SciString;
import javasci.SciStringArray;
import javasci.Scilab;
import javasciplugin.plot.Plot;

import org.eclipse.core.runtime.IPath;


/**
 * This class is a wrapper class for the access to Scilab functions. It is the main connection between the plugin and the Scilab engine.
 * The methods are all synchronized to prevent deadlocks when multiple plugin widgets access the wrapper at the same time.
 * The main methods are loadHistory, loadSciWorkspace and exec.
 * **/
public class JavaSciWrapper {
	
	private SciStringArray javaSciMatrix1;
	private SciStringArray javaSciMatrix2;
	private SciStringArray javaSciMatrix3;
	private SciStringArray javaSciMatrix4;
	private SciStringArray javaSciMatrix1Size;
	private SciString javaSciVariable;
	private SciString javaSciVariableSize;
	
	private Vector<String> historyVector;
	private Vector<String> workspaceVector;
	
	private Plot plot;
	private final Semaphore plotSemaphore = new Semaphore(1);
	
	private int SciHistoryCount;
	private IPath fileName;
	private Vector<Observer> observerVector;
	
	
	/**
	 *The constructor initialises the Scilab variables. 
	 *All the Scilab variables that are may be needed by the plugin are initialised.
	 *Furthermore the observer vector is initialised. 
	 *This vector contains all the views that need to be notified when a command is executed (history, workspace).  
	 * **/
	public JavaSciWrapper(){
		Scilab.Exec("javaSciVariableName=\"\"\"\"");
		Scilab.Exec("javaSciVariableValue=\"\"\"\"");
		javaSciMatrix1 = new SciStringArray("javaSciMatrix1",1,1);
		javaSciMatrix2 = new SciStringArray("javaSciMatrix2",1,1);
		javaSciMatrix3 = new SciStringArray("javaSciMatrix3",1,1);
		javaSciMatrix4 = new SciStringArray("javaSciMatrix4",1,1);
		javaSciMatrix1Size = new SciStringArray("javaSciMatrix1Size",1,1);
		javaSciVariable = new SciString("javaSciVariable");
		javaSciVariableSize = new SciString("javaSciVariableSize");
		observerVector=new Vector<Observer>();
	}

	/**
	 *This method fills a global String Vector with the command history that is saved in two different places.
	 *The two files are the Scilab history and a history file that is located in the working directory of the plugin.
	 *Furthermore the size of the Scilab history is saved.
	 * **/
	public synchronized void loadSciHistory(){

		Scilab.Exec("javaSciMatrix1Size=string(size(gethistory()));");		
		Scilab.Exec("javaSciMatrix1=string(gethistory())");

		int size = Integer.parseInt(javaSciMatrix1Size.GetElement(1, 1));
	    historyVector=new Vector<String>(size);
	    for(int i=size; i>=1; i--)   {
	    	historyVector.add(javaSciMatrix1.GetElement(i, 1).trim());
	    }
	    SciHistoryCount = historyVector.size();
	    
	    fileName=Activator.getDefault().getHistoryFile();
	    try {
	        BufferedReader in = new BufferedReader(new FileReader(fileName.toOSString()));
	        String str;
	        while ((str = in.readLine()) != null) {
	        	historyVector.add(0,str);
	        }
	        in.close();
	    } catch (IOException e) {
	    }
	}
	
	/**
	 * This method returns the history from the history Vector.
	 * The history is loaded into the history Vector with the method loadHistory.
	 * @return an Enumeration that contains the history from the history Vector.
	 * **/
	public synchronized Enumeration getHistory(){
		return historyVector.elements();
	}
	
	/**
	 *This method notifies to all history and workspace views that a command was executed.
	 * The command is added to the history Vector. 
	 * **/
	private synchronized void updateViews(String newElement){
		historyVector.add(0,newElement);
		loadSciWorkspace();
		for(int i=0;i<observerVector.size();i++){
			((Observer)observerVector.elementAt(i)).update(this);
		}
	}
	
	/**
	 *This method returns a command from the history. This command is identified by an index and a starting string.
	 *If the starting string is empty then the element of the history Vector at the position of the index is returned. 
	 *Else only the elements with the particular starting string at there beginning are considered.
	 *Furthermore the index will than identifies the n's command with this starting string.
	 *This method is used by the console when the arrows up and down are pressed.
	 *@param index is the n's occurrence of a command with a particular starting string
	 *@param startingString is the string with what a element of the history Vector must start  
	 *@return a string from the history Vector which starts with the startingString and is the n's occurrence in the history. 
	 * **/
	public synchronized String getHistoryElement(int index, String startingString){
		if(startingString.compareTo("")== 0){
			if(index >= historyVector.size() || index < 0){
				return null;
			}
			else{
				return (String)historyVector.elementAt(index);
			}
		}
		else{
			int i = 0;
			int count = 0;
			String previews="";
			
			while(i < historyVector.size() && count <= index){
				if(historyVector.get(i).indexOf(startingString)==0 && historyVector.get(i).compareTo(previews) != 0){
					count++;
					previews=historyVector.get(i);
				}
				i++;
			}
			if(i < historyVector.size() && index > -1){
				return historyVector.get(i-1);
			}else{
				return null;
			}
		}
			
	}

	/**
	 * This method adds an observer to the list of observers that are notified when a command is executed.
	 * @param o is an instance of a class that implements the interface Observer (HistoryWidget and workspaceWidget)
	 * **/
	public synchronized void addObserver(Observer o) {
		if(!observerVector.contains(o)){
			observerVector.add(o);
		}	
	}

	/**
	 * This method is called by the Activator when it is closed. It sends a close signal to the plot thread.
	 * Furthermore it saves the command that are not in the Scilab history to a file inside the plugin working directory because the plugin can not add commands to the Scilab history.
	 * In the end it closed the Scilab engine. 
	 **/
	public synchronized void dispose(){
		if (plot != null){
			plot.close();
		}
		try {
	        BufferedWriter out = new BufferedWriter(new FileWriter(fileName.toOSString()));
	        for(int i=historyVector.size()-SciHistoryCount; i >= 0 ; i--)   {
	        	out.write((String)historyVector.elementAt(i));
	        	out.newLine();
		    }
	        out.close();
	    } catch (IOException e) {
	    }
	    Scilab.Finish();
		
	}
	
	/**
	 * This method converts the integer code if a Scilab variable type into the respective human readable string.
	 * @param code is the code of the Scilab variable type
	 * @return string of the Scilab variable type
	 * **/
	private String getVariableType(int code){
		
		switch(code){
			case -1: return("None");
			case 1: return("real or complex constant matrix"); 
			case 2: return("polynomial matrix");
			case 4: return("boolean matrix");
			case 5: return("sparse matrix");
			case 6: return("sparse boolean matrix.");
			case 8: return("matrix of integers stored on 1 2 or 4 bytes"); 
			case 9: return("matrix of graphic handles"); 
			case 10: return("matrix of character strings");
			case 11: return("un-compiled function"); 
			case 13: return("compiled function");
			case 14: return("function library");
			case 15: return("list");
			case 16: return("typed list");
			case 17: return("mlist");
			case 128: return("pointer");
		}
		return null;
	}
	
	/**
	 * This method returns a semaphore that is used with the plot function.
	 * The plot function must not be used in parallel with the loadSciWorkspace method otherwise a xlib error may occur.
	 * @return a semaphore that is used in with the plot function
	 * **/
	public synchronized Semaphore getPlotSemaphor(){
		return plotSemaphore;
	}
	
	/**
	 * This method loads the current workspace from the Scilab engine.
	 * The currently user allocated variables are saved with there name, value (first element of the array), type and size in the workspace Vector.
	 * THis method is executed at the initialisation and at every command execution, except when this command is a plot command. The plot command and the workspace command are mu not be used in parallel. 
	 * **/
	public synchronized void loadSciWorkspace(){
		
		if(plotSemaphore.tryAcquire()){
				
			int size = 0;
			
			Scilab.Exec("[javaSciVariableName, javaSciVariableValue]=who(\"\"local\"\");");
			Scilab.Exec("javaSciMatrix1=string(javaSciVariableName);");
			Scilab.Exec("javaSciMatrix1Size=string(size(javaSciVariableName));");
		    Scilab.Exec("javaSciMatrix1=string(javaSciVariableName);");
		   
		    size = Integer.valueOf(javaSciMatrix1Size.GetElement(1, 1)).intValue();
		    
		    int i=0;
		    while(javaSciMatrix1.GetElement(i,1).compareTo("home")!=0 && i <= size){
		    	i++;
		    }
		    size=i;
		    workspaceVector=new Vector<String>();
		
		    int variableType =-1;
		    String dim="";
	
			for (i = 1; i < size; i++) {
				if(javaSciMatrix1.GetElement(i,1).indexOf("javaSci")==-1 && javaSciMatrix1.GetElement(i,1).indexOf("TMP_EXEC_STRING")==-1){	
					
					Scilab.Exec("javaSciVariable=javaSciMatrix1("+ i +");");
					variableType = Scilab.TypeVar(javaSciVariable.getData());
					String value="";
					
					if(variableType != 11 && variableType != 13 && variableType != 14 && variableType != -1){
						Scilab.Exec("javaSciMatrix3=string("+ javaSciVariable.getData() +");");		
						Scilab.Exec("javaSciVariableSize=string(length(size("+ javaSciVariable.getData() +")));");
						Scilab.Exec("javaSciMatrix4=string(size("+ javaSciVariable.getData() +"));");
						dim = javaSciMatrix4.GetElement(1, 1);
						for(int j=2;j<=Integer.parseInt(javaSciVariableSize.getData());j++){
							dim= dim +" x "+javaSciMatrix4.GetElement(j, 1);
						}
						value =javaSciMatrix3.GetElement(1, 1);
					}else{
						value = "-";
						dim = "-";
					}
	
					workspaceVector.add(javaSciMatrix1.GetElement(i,1));
					workspaceVector.add(value);
					workspaceVector.add(getVariableType(variableType));
					workspaceVector.add(dim);
				}
			}	
			size=i;
			plotSemaphore.release();
		}
	}
	
	/**
	 * This method returns the workspace Vector witch contains the current user defined variables by there name, first value, type and size.
	 * The Vector is filled with the loadSciWorkspace method.
	 * @return the workspace Vector witch contains the current user defined variables by there name, first value, type and size
	 * **/
	public synchronized Vector getSciWorkspace(){
		return workspaceVector;
	}
	
	
	/**
	 * This method executes a command that is passed as parameter and returns a Vector that contains the output of this command. This method is used by the console.
	 * The command is normally executed with the string function of Scilab. This makes it possible to save the output of the command in a matrix of strings. 
	 * However some commands do not work with the string function: clear, exec, multiple lined commands, =, plot and the visualisation functions of Scilab: disp, printf.
	 * The plot is executed in a separate thread of the type Plot. A multiple lined command is temporary saved in a file and this file is executed as a normal script.
	 * After the execution of the command the result formated so that the digits are placed one below each other in a matrix.
	 * This formatted text is saved in the return Vector.
	 * Known restriction: plot command can not be used in a script. 
	 * @param pCommand is the command that should be executed.
	 * @return a Vector that contains the output of a command.
	 * **/
	public synchronized Vector exec(String pCommand){
		
		String command=pCommand;
		command.replaceAll("'", "\"\"");
		boolean execError=true;
		Vector<String> resultVector= new Vector<String>();
		
		if(command.indexOf('=')== -1 && command.indexOf("plot") == -1 && command.indexOf('\n')==-1 && command.indexOf("exec")==-1 && command.indexOf("clear")==-1){
			execError = Scilab.Exec("javaSciMatrix1=string("+command+");");
		}
		else if(command.indexOf("exec")>-1){
			execError = Scilab.Exec(command);
			Scilab.Exec("javaSciMatrix1 = [\"\"executed\"\"];");	
		}
		else if(command.indexOf("clear")>-1){
			execError = Scilab.Exec(command);
			Scilab.Exec("javaSciMatrix1 = [\"\"cleared\"\"];");			
		}
		else if(command.indexOf('\n')>-1){
			IPath tempFileName=Activator.getDefault().getTempFile();
			try {
		        File deleteFile = new File(tempFileName.toOSString());
		        deleteFile.delete();
		        deleteFile=null;
		        
				BufferedWriter out = new BufferedWriter(new FileWriter(tempFileName.toOSString()));
		        out.write(command);
		        out.close();
		    } catch (IOException exception) {
		    }
			execError = Scilab.Exec("exec(\"\""+tempFileName.toOSString()+"\"\");");
			Scilab.Exec("javaSciMatrix1 = [\"\"executed\"\"];");
		}
		else if(command.indexOf("==") != command.indexOf("=")){
		
			execError = Scilab.Exec(command);
			Scilab.Exec("javaSciMatrix1 = [\"\"assigment\"\"];");
		}
		else if(command.indexOf("plot") > -1 ){
			if (plot == null){
				System.out.println("plot = new PlotEvent();");
				plot = new Plot();
				plot.plot(command);
				plot.setJavaSciWrapper(this);
				plot.start();
			}else{
				plot.plot(command);
			}
			execError=true;
			Scilab.Exec("javaSciMatrix1 = [\"\"plot...\"\"];");
		}
		
		
		if(execError){
			
			int beforeComma=0;
			int afterComma=0;			
			Scilab.Exec("javaSciMatrix2=string(size(javaSciMatrix1));");
			
			for(int i = 1;i< Integer.parseInt(javaSciMatrix2.GetElement(1, 1))+1;i++){
				for(int j = 1;j<Integer.parseInt(javaSciMatrix2.GetElement(2, 1))+1;j++){
					
					int indexOfPoint=javaSciMatrix1.GetElement(i, j).indexOf(".");
					int length=javaSciMatrix1.GetElement(i, j).length();
					
					if(length - indexOfPoint > afterComma && indexOfPoint > -1){
						afterComma = length - indexOfPoint;
					}
					if(indexOfPoint > beforeComma && indexOfPoint > -1){
						beforeComma = indexOfPoint;
					}
					if(length > beforeComma && indexOfPoint == -1){
						beforeComma = length;
					} 
				}
			}

			for(int i = 1;i< Integer.valueOf(javaSciMatrix2.GetElement(1, 1)).intValue()+1;i++){
				String oneLine="";
				for(int j = 1;j<Integer.valueOf(javaSciMatrix2.GetElement(2, 1)).intValue()+1;j++){					
					
					String value = javaSciMatrix1.GetElement(i, j);
					String spacesBefore="";
					String spacesAfter="";
				
					if (value.indexOf(".") > -1){
						for(int k = value.indexOf(".");k < beforeComma;k++){
							spacesBefore=spacesBefore+" ";
						}
						for(int k = value.length() - value.indexOf(".")-1;k < afterComma;k++){
							spacesAfter=spacesAfter+" ";
						}
					}
					else{
						for(int k = value.length();k < beforeComma;k++){
							spacesBefore=spacesBefore+" ";
						}
						for(int k = 0;k <= afterComma;k++){
							spacesAfter=spacesAfter+" ";
						}
					}
					oneLine = oneLine + spacesBefore + value + spacesAfter;
				}
				resultVector.add(oneLine);
			}
		}
		else{
			resultVector = null;
		}
		
		updateViews(command);
		return resultVector;
		
	}
	

}
