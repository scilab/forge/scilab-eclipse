package javasciplugin.console;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.IPersistableElement;

/**
 * This class describes the EditorInput for the Console. The Console is a editor and in eclipse every editor needs an input. 
 * This input is normally a file but in our case there is no file thats why a EditorInput needs to be created. 
 * The main purpose of this input is to make sure that only one Editor with the same input is open at the time.
 * **/
public class myEditorInput implements IEditorInput{
	
	/**
	 * This method is a default eclipse method.
	 * **/
	public boolean exists() {
		return false;
	}
	
	/**
	 *  This method is a default eclipse method.
	 * **/
	public ImageDescriptor getImageDescriptor() {
		return null;
	}
	
	/**
	 * This method returns the Name
	 * @return Name
	 * **/
	public String getName() {
		return "Console";
	}

	/**
	 * This method ensures that the console will not lose state during multiple starts of eclipse.
	 * The position and size of the console are saved.
	 * @return ElementFactory
	 * **/
	public IPersistableElement getPersistable() {
		return new IPersistableElement() {
			public String getFactoryId() {
				return "javasciplugin.ElementFactory";
			}
			public void saveState(IMemento memento) {
			}
		};
	}
	
	/**
	 * This method returns the ToolTipText
	 * @return ToolTipText
	 * **/
	public String getToolTipText() {
		return "SciConsole";
	}
	
	/**
	 *  This method is a default eclipse method.
	 * **/
	public Object getAdapter(Class adapter) {
		return null;
	}
	
	/**
	 * This method indicates if some input equals this input. If the input is of the type myEditorInput it returns true.
	 * @param obj is the input that is tested.
	 * @return ture if the parameter is of the type myEditorInput else false.
	 * **/
	public boolean equals(Object obj){
		if (obj instanceof myEditorInput){
			return true;
		}
		return false;
	}

}
