package javasciplugin;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.ui.IElementFactory;
import org.eclipse.ui.IMemento;

import javasciplugin.console.myEditorInput;

/**
 * This class is used with the getPersistable method of the myEditorInput.
 * It is used to ensure that the console does not lose state during multiple starts of eclipse.
 * **/
public class ElementFactory implements IElementFactory {

	public IAdaptable createElement(IMemento memento) {
		return new myEditorInput();
	}


}
