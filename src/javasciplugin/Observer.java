package javasciplugin;

/**
 * This Interface is used by the HistoryWidget and the WorkspaceWidget to get notified of command executions.
 * **/
public interface Observer {
	/**
	 * This method is implemented by the HistoryWidget and the WorkspaceWidget to update there views after a command execution.
	 * **/
    public void update(JavaSciWrapper pWrapper);
}
