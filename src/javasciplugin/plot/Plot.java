package javasciplugin.plot;

import javasci.Scilab;
import javasciplugin.JavaSciWrapper;

/**
 * Plot is a class that is used to show Scilab plots in a separate window. 
 * **/
public class Plot extends Thread
{
	private String command;
	private boolean closePlot;
	private boolean closeGraph;
	private JavaSciWrapper wrapper;
	
	/**
	 * This method is the main thread method. It is started when the tread is started. It will loop until the variable closePlot is true.
	 * The loop has three states. If no graph is currently active and the command string is empty the tread sleeps for 100 millisecond.
	 * If the command string is not empty the thread waits until it gets the plotSemaphore and executes the command.
	 * If a graph is active the tread handles the graph events to the Scilab engine until the graph closes.
	 * The semaphore is needed to prevent concurrent access of the loadSciWorkspace method from the JavaSciWrapper and the plot onto the same variables.
	 * This concurrent access can cause a Xlib crash.
	 * **/
	public void run()
	{
		closePlot=false;
		closeGraph=false;
		do 
		{
			if(Scilab.HaveAGraph() != false){
				command=null;
				closeGraph=true;
				Scilab.Events();
			}else{
				if (closeGraph){
					Scilab.Events();
					closeGraph=false;
					wrapper.getPlotSemaphor().release();
				}
				if(command == null){
					try {
						Plot.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				else{
						while(!wrapper.getPlotSemaphor().tryAcquire()){
							try {
								Plot.sleep(100);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
						Scilab.Exec(command);
				}
			}	
		}
		while (!closePlot);
		
		Scilab.Events();
	}
	
	/**
	 * This method is used to set the plot command. This command is only executed when no other plot is active.
	 * @param pCommand is a string that contains the command
	 * **/
	public void plot(String pCommand){
		command = pCommand;
	}
	
	/**
	 * This method is used to terminate the plugin. It sets the variable that is used in the run loop.
	 * **/
	public void close(){
		closePlot=true;
	}
	
	/**
	 * This method is used to set a reference to the javaSciWrapper in order to get access to the plotSemaphore.
	 * @param pJavaSciWrapper is the JavaSciWrapper
	 * **/
	public void setJavaSciWrapper(JavaSciWrapper pJavaSciWrapper){
		wrapper = pJavaSciWrapper;
	}
}
